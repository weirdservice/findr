#!/usr/bin/env node
import program from 'commander'
import request from 'request'
import _ from 'lodash'
import {DomHandler, Parser} from 'htmlparser2'
import {EventEmitter} from 'events'

import pkg from '../package.json'

const TYPE_ATOM = 'application/atom+xml'
const TYPE_RSS = 'application/rss+xml'

program
  .version(pkg.version)
  .usage('<url>')
  .parse(process.argv)

if (!program.args.length) {
  program.outputHelp()
  process.exit()
}

const url = program.args[0]

const error = (err) => {
  console.log(JSON.stringify({status: "failed", url: url, message: err.message}))
}

const success = (data) => {
  data.status = 'success'
  console.log(JSON.stringify(data))
}

const handler = new DomHandler((error, dom) => {
  if (error) return findr.emit('error', new Error(error))

  let links = filter(dom.pop().children)
  return findr.emit('parse', links)
})

const filter = (data) => {
  return _.filter(
    data,
    (tag) => {
      return tag.name === 'link' && (
        tag.attribs.type === TYPE_ATOM ||
        tag.attribs.type === TYPE_RSS )
    }).map(
      (link) => {
        return {
          title: link.attribs.title,
          href: link.attribs.href
        }
      }
  )
}

let findr = new EventEmitter

findr.on('error', error)
findr.on('parse', (links) => {
  if(links.length == 0)
    return findr.emit('error', new Error('Feed URL doesn\'t exist'))
  success({url: url, feeds: links})
})

// dirty URL check
if (url.split('://').length < 2) {
  findr.emit('error', new Error('URL is invalid'))
  process.exit()
}

const req = request(url)
const parser = new Parser(handler, {decodeEntities: true})

// naughty collect
var stream = ''

req.on('error', (error) => {
  findr.emit('error', error)
})

req.on('response', (res) => {
  if (res.statusCode != 200) {
    req.emit('error', new Error(`Bad status ${res.statusCode}`))
    process.exit()
  }
})

req.on('data', (res) => {
  stream += res.toString()
})

req.on('end', () => {
  try {
    let head = stream.match(/<head[^>]*>[\s\S]*<\/head>/gi)
    parser.write(head)
    parser.end()
  } catch (e) {
    return req.emit('error', new Error(e.message))
  }
})
