#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _htmlparser2 = require('htmlparser2');

var _events = require('events');

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

var TYPE_ATOM = 'application/atom+xml';
var TYPE_RSS = 'application/rss+xml';

_commander2['default'].version(_packageJson2['default'].version).usage('<url>').parse(process.argv);

if (!_commander2['default'].args.length) {
  _commander2['default'].outputHelp();
  process.exit();
}

var url = _commander2['default'].args[0];

var error = function error(err) {
  console.log(JSON.stringify({ status: "failed", url: url, message: err.message }));
};

var success = function success(data) {
  data.status = 'success';
  console.log(JSON.stringify(data));
};

var handler = new _htmlparser2.DomHandler(function (error, dom) {
  if (error) return findr.emit('error', new Error(error));

  var links = filter(dom.pop().children);
  return findr.emit('parse', links);
});

var filter = function filter(data) {
  return _lodash2['default'].filter(data, function (tag) {
    return tag.name === 'link' && (tag.attribs.type === TYPE_ATOM || tag.attribs.type === TYPE_RSS);
  }).map(function (link) {
    return {
      title: link.attribs.title,
      href: link.attribs.href
    };
  });
};

var findr = new _events.EventEmitter();

findr.on('error', error);
findr.on('parse', function (links) {
  if (links.length == 0) return findr.emit('error', new Error('Feed URL doesn\'t exist'));
  return success({ url: url, feeds: links });
});

// dirty URL check
if (url.split('://').length < 2) {
  findr.emit('error', new Error('URL is invalid'));
  process.exit();
}

var req = (0, _request2['default'])(url);
var parser = new _htmlparser2.Parser(handler, { decodeEntities: true });

// naughty collect
var stream = '';

req.on('error', function (error) {
  findr.emit('error', error);
});

req.on('response', function (res) {
  if (res.statusCode != 200) {
    req.emit('error', new Error('Bad status ' + res.statusCode));
    process.exit();
  }
});

req.on('data', function (res) {
  stream += res.toString();
});

req.on('end', function () {
  try {
    var head = stream.match(/<head[^>]*>[\s\S]*<\/head>/gi);
    parser.write(head);
    parser.end();
  } catch (e) {
    return req.emit('error', new Error(e.message));
  }
});
